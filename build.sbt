organization := "com.pongr"

name := "pongr-personalized-feed"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.9.1"

credentials += Credentials(Path.userHome / ".ivy2" / ".pongr_credentials")

resolvers in ThisBuild ++= Seq(
  "Pongr" at "http://internal.nexus.pongrdev.com:8081/nexus/content/groups/public",
  "Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
	"Pongr Releases" at "http://pongrdev.com:8081/nexus/content/repositories/releases",
  "Typesafe" at "http://repo.typesafe.com/typesafe/releases/"  
)

libraryDependencies ++= Seq(
  "com.yammer.metrics" %% "metrics-scala" % "2.0.3",
  "commons-codec" % "commons-codec" % "1.6",
  "com.pongr" %% "pongr-views-squeryl" % "0.252.0",
  "com.pongr" %% "pongr-views-redis" % "0.252.0",
  "com.pongr"    %% "pongr-oauth2" % "1.2.0",
  "com.typesafe.akka" % "akka-actor" % "2.0.4",
  "com.pongr" %% "pongr-simpledb" % "1.5",
  "com.pongr" %% "pongr-domain-command-handlers" % "0.192.0"
)