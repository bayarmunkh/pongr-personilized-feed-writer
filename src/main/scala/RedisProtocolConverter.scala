package com.pongr

object RedisProtocolConverter {


  // Response codes from the Redis server
  val ERR    = '-'
  val OK     = "OK".getBytes("UTF-8")
  val QUEUED = "QUEUED".getBytes("UTF-8")
  val SINGLE = '+'
  val BULK   = '$'
  val MULTI  = '*'
  val INT    = ':'

  val LS     = "\r\n".getBytes("UTF-8")

  def multiBulk(cmd: String, key: String, score: Long, value: Int): Array[Byte] = {
  	val s = Seq(cmd.getBytes("UTF-8"), key.getBytes("UTF-8"), score.toString.getBytes("UTF-8"), value.toString.getBytes("UTF-8"))
  	multiBulk(s)
  }

	def multiBulk(args: Seq[Array[Byte]]): Array[Byte] = {
    val b = new scala.collection.mutable.ArrayBuilder.ofByte
    b ++= "*%d".format(args.size).getBytes
    b ++= LS
    args foreach { arg =>
      b ++= "$%d".format(arg.size).getBytes
      b ++= LS
      b ++= arg
      b ++= LS
    }
    b.result
  }	
}