package com.pongr

import com.pongr._
import com.redis._
import com.redis.RedisClient._

import com.yammer.metrics.scala.Instrumented
import serialization._

import com.pongr.view.redis._
import com.pongr.view.model.Schemas

import org.squeryl._
import org.squeryl.PrimitiveTypeMode._

import grizzled.slf4j.Logging

import java.text.SimpleDateFormat
import java.sql.Timestamp
import RedisFactory._

object ImportFeeds extends SquerylHelper with Instrumented with RedisHelper with Logging {

	//Staging Config
	//val redisServer = "redis1.pongrdev.com"
  //Production Config
  val redisServer = "redis3.pongrdev.com"
  //val redisServer = "184.72.196.39"
  //val redisServer = "ec2-184-72-196-39.compute-1.amazonaws.com"
	Settings.redisConfig = Some(RedisConfig(redisServer, 6379, 1))

  //val filterDateTime = "2013-04-26 04:50:00"
  //val filterDateTime = "2013-05-29 00:00:00"
  val filterDateTime = "2013-06-07 00:00:00"
  val since = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(filterDateTime).getTime)


  //override lazy val clients = new RedisClientPool("redis1.pongrdev.com", 6379, database = 1) //staging
  val clients = new RedisClientPool(redisServer, 6379, database = 1) //production
  val client = {
    clients.withClient { c => c }
  }

	def main(args: Array[String]) {

		val excludedBrandsList = Seq(
		  "4299e495-2efe-4140-a868-7e005a50ab1c", 
		  "86e5121c-70a4-44fb-87d3-e5f58476d013",
		  "029a9232-237a-4354-8da5-0a577e42ca65",
		  "be9b0f2f-2e81-4a43-bce5-f4fa9272e7ea",
		  "671d22e8-909a-4dcd-bf3c-23fb5344bef9",
		  "e1b2e8fb-09b1-49d3-a123-f065dc221b22"
		)
		val problemUsersList = Seq("D1ZMHBUNY1DJOFP0WPE3XOX2BAXA3DMD", "H4424SKXYMQ4XDR1SG2WD4A31BAPQSCZ")

		initSqueryl

		var countentity = 0

		inTransaction {

			//client.sadd(problemUsersTable, problemUsersList.head, problemUsersList.tail: _*)
			//client.sadd(excludedBrandsTable, excludedBrandsList.head, excludedBrandsList.tail: _*)

			val photoIds: Map[String, String] = client.hgetall(photoIdsTable).get
			val photos = from(Schemas.photos)(p => where(p.createdAt.getOrElse(null) >= since) select((p.uuid, p.brand, p.user, p.createdAt.map(_.getTime.toDouble).getOrElse(0d))) orderBy(p.createdAt desc)).toList
			//where(u.id in List(1, 7, 11, 799, 849))
			val userIds: Map[Long, String] = from(Schemas.users)((u) => select(u.id, u.uuid) orderBy(u.createdAt asc)).toMap//.toSeq.sortWith((a,b) => photoUserIds.indexOf(a._1) < photoUserIds.indexOf(b._1)).toMap
			//println("Make sure users are ordered correctly : " + userIds.take(10))
			val userIntIds: Map[String, Long] = from(Schemas.users)((u) => select(u.uuid, u.id)).toMap
			val brandIds: Map[Long, String] = from(Schemas.brands)((b) => select(b.id, b.uuid)).toMap
			val brandIntIds: Map[String, Long] = from(Schemas.brands)((b) => select(b.uuid, b.id)).toMap
			//val photoIds: Map[String, Int] = photos.map{_._1}.zipWithIndex.toMap

			var brandcount = 0
			//Importing Brand Feeds
			val brandPhotos: Map[Long, Seq[(String, Long, Long, Double)]] = photos.groupBy(_._2)
			info("Brand feeds count : " + brandPhotos.size)
			for((brandId, photos) <- brandPhotos.toList) {
				if(countentity % 10000 == 0) info("importing brand feed : " + countentity)
				for(ps <- photos.grouped(10000).toList) {
					//if(brandId == 614l) {info("614 : " + brandcount + ", SIZE : " + ps.size + ", ID: " + brandIds.getOrElse(brandId, "null")); brandcount += 1}
					ps.map{p => (p._4, p._1)}.toList match {
						case head::tail =>
							client.zadd(brandFeedTable(brandIds.getOrElse(brandId, "null")), head._1, photoIds.getOrElse(head._2, 0), tail.map{p => (p._1, photoIds.getOrElse(p._2, 0))}: _*)
						case _ =>
					}
				}
				countentity += 1
			}
			info("imported brand feeds.")



			//Importing User Feeds
			countentity = 0
			val userPhotos: Map[Long, Seq[(String, Long, Long, Double)]] = photos.groupBy(_._3)
			info("User feeds count : " + userPhotos.size)
			for((userId, userPhotos) <- userPhotos.toList) {
				if(countentity % 10000 == 0) info("importing user feed : " + countentity)
				userPhotos.map{p => (p._4, p._1)}.toList match {
					case ps if(ps.size > 0) =>
						val head::tail = ps
						client.zadd(userFeedTable(userIds.getOrElse(userId, "null")), head._1, photoIds.getOrElse(head._2, 0), tail.map{p => (p._1, photoIds.getOrElse(p._2, 0))}: _*)
					case _ =>
				}
				countentity += 1
			}			
			info("imported user feeds.")



			//Import Full Photo Feeds
			countentity = 0
			for(ps <- photos.grouped(10000).toList) {
				if(countentity % 10000 == 0) info("importing full feed : " + countentity)
				ps.map{p => (p._4, p._1)}.toList match {
					case head::tail =>
						client.zadd(fullPhotoFeedTable, head._1, photoIds.getOrElse(head._2, 0), tail.map{p => (p._1, photoIds.getOrElse(p._2, 0))}: _*)
					case _ =>
				}
				countentity += 1
			}
			info("imported full feed.")



			//Import Home Feeds
      val dbuserfollowers = {
      	join(Schemas.userFollows, Schemas.users, Schemas.users)((uf, u1, u2) =>
          select(u1.uuid, u2.uuid)
          on(uf.likedUser === u1.id, uf.likedByUser === u2.id)
        ).toList
      }
      val userfollowings = dbuserfollowers.groupBy(_._1)

      val dbbrandlovers = {
        join(Schemas.brandLoves, Schemas.brands, Schemas.users)((bl, b, u) =>
          select(b.uuid, u.uuid)
          on(bl.brand === b.id, bl.user === u.id)
        ).toList
      }
      val brandlovers = dbbrandlovers.groupBy(_._2)

      //Read User Blocks
      val dbuserblocks = {
        join(Schemas.userBlocks, Schemas.users, Schemas.users)((ub, u1, u2) =>
          select(u1.uuid, u2.uuid)
          on(ub.blockedUser === u1.id, ub.blockedByUser === u2.id)
        ).toList
      }

      val homeFeedPhotos = from(Schemas.photos)(p => where(p.createdAt.getOrElse(null) >= since and (p.brand notIn List(343l, 583l))) select((p.uuid, p.brand, p.user, p.createdAt.map(_.getTime.toDouble).getOrElse(0d))) orderBy(p.createdAt desc)).toList

			val homeFeedLimit = 1000
			countentity = 0
			//Problem users photos
			val problemUsersAsInt = problemUsersList.map(userIntIds.get(_)).flatten
			val brandPhotosWithoutProblemUsers: Map[Long, Seq[(String, Long, Long, Double)]] = homeFeedPhotos.filter(a => !problemUsersAsInt.contains(a._3)).groupBy(_._2)

			for((userIdAsInt, userId) <- userIds) {
				if(countentity % 10000 == 0) info("generating home feed1 : " + countentity)
				val photos = {

					(userfollowings
						.getOrElse(userId, Nil).map(_._2) :+ userId)
						.filter(u => !dbuserblocks.contains(userId -> u)) //the user blocked the follower, don't add
						.map{ u =>
							userIntIds.get(u).map{id => userPhotos.getOrElse(id, Nil).map(p => (p._1, p._4)).take(homeFeedLimit)}
					}.flatten.foldLeft(Seq[(String, Double)]())(_++_) ++
					brandlovers
						.getOrElse(userId, Nil)
						.map(_._1)
						.filter(b => !excludedBrandsList.contains(b)) //filter the excluded brand photos out
						.map{ b => 

							brandIntIds.get(b).map{id => 
								brandPhotosWithoutProblemUsers
									.getOrElse(id, Nil)
									//.filter(!problemUsersPhotos.contains(_)) //filter the problem users photos out
									.map(p => (p._1, p._4))
									//.sortWith(_._2>_._2)
									.take(homeFeedLimit)
							}
					}.flatten.foldLeft(Seq[(String, Double)]())(_++_)
				}.distinct.sortWith(_._2>_._2).take(homeFeedLimit)
				photos match {
					case head::tail =>
						client.zadd(homeFeedTable(userId), head._2, photoIds.getOrElse(head._1, 0), tail.map{p => (p._2, photoIds.getOrElse(p._1, 0))}: _*)
					case _ =>
				}
				countentity += 1
			}
			info("generated home feed.")


		}
	}

}