package com.pongr

import com.pongr._
import com.redis._

import com.yammer.metrics.scala.Instrumented
import serialization._

import com.pongr.view.model.{PhotoDb => PhotoTable, CommentDb => SqlCommentDb, _}
import com.pongr.view.model.CommentType._
import com.pongr.view.photo._
import com.pongr.view.redis._
import com.pongr.view.common._
import com.pongr.domain.commandhandlers._
import com.pongr.oauth2._
import com.pongr.simpledb._
import java.util.Date
import java.util.UUID._

import org.squeryl._
import org.squeryl.PrimitiveTypeMode._

import com.amazonaws.services.simpledb.AmazonSimpleDBClient
import com.amazonaws.auth.BasicAWSCredentials

import grizzled.slf4j.Logging
import com.redis.serialization.Parse.Implicits.parseInt

import java.io._

import com.amazonaws.services.simpledb.AmazonSimpleDB
import com.amazonaws.services.simpledb.model.{PutAttributesRequest, SelectRequest, ReplaceableAttribute, Item}

import java.text.SimpleDateFormat
import java.sql.Timestamp

import RedisFactory._

import org.joda.time._


object FixPhotoComments extends SquerylHelper with Instrumented with RedisHelper with Logging {

  //Staging Config
  //val redisServer = "54.242.224.74"
  //Production Config
  //val redisServer = "184.72.196.39"
  val redisServer = "redis3.pongrdev.com"
  Settings.redisConfig = Some(RedisConfig(redisServer, 6379, 1))
  
  val filterDateTime = "2013-04-26 04:50:00"
  //val filterDateTime = "2013-06-07 00:00:00"
  val since = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(filterDateTime).getTime)

  val clients = new RedisClientPool(redisServer, 6379, database = 1) //production
  val r = {
    clients.withClient { c => c }
  }

  val campaignQuery = new InMemoryCampaignQuery

  def main(args: Array[String]) {

    try {

      initSqueryl

      inTransaction {
      
        //get photos from Redis
        val photoIds: Map[String, String] = r.hgetall[String, String](photoIdsTable).get
        val dbcommentsByPhoto: Map[String, Seq[CommentDb]] = {
          join(Schemas.comments, Schemas.users, Schemas.photos)((c, u, p) =>
            where(c.typeofComment === CommentType.Photo and c.createdAt >= since)          
            select(c, u.uuid, p.uuid)
            on(c.user === u.id, c.commentable === p.id)
          ).toSeq.map{ v =>
            val (c, userId, photoId) = v
            photoId -> CommentDb(c.id, randomUUID.toString, c.text, userId, c.createdAt)        
          }.groupBy(_._1) mapValues (_ map (_._2))
        }

        //val dbcomments: Seq[CommentDb] = dbcommentsByPhoto.values.reduceLeft{_++_}
        for(list <- dbcommentsByPhoto.values.grouped(1000).toList) {
          val dbcomments: Seq[CommentDb] = list.reduceLeft{_++_}
          if(dbcomments.nonEmpty)
            r.hmset(commentsTable, dbcomments.map{c => (c.uuid, write(c))})
        }
        info("imported comments.")        

        for{
          photoUuid <- dbcommentsByPhoto.keys
        } {
          for(photoId <- r.hget(photoIdsTable, photoUuid)) {
            hupdate[com.pongr.view.redis.PhotoDb](photosTable(photoId), photoId.toString, p => p.copy(comments = dbcommentsByPhoto.getOrElse(p.uuid, Nil).map{_.uuid}.toList))
          }
        }

        info("Done...")
    
      }
    } catch {
      case e: Exception =>
        println(e.getMessage)
    }

    System.exit(0)

  }


}