package com.pongr

import com.pongr._
import com.redis._

import com.yammer.metrics.scala.Instrumented
import serialization._

import com.pongr.view.model.{PhotoDb => PhotoTable, CommentDb => SqlCommentDb, _}
import com.pongr.view.model.CommentType._
import com.pongr.view.photo._
import com.pongr.view.redis._
import com.pongr.view.common._
import com.pongr.domain.commandhandlers._
import com.pongr.oauth2._
import com.pongr.simpledb._
import java.util.Date
import java.util.UUID._

import org.squeryl._
import org.squeryl.PrimitiveTypeMode._

import com.amazonaws.services.simpledb.AmazonSimpleDBClient
import com.amazonaws.auth.BasicAWSCredentials

import grizzled.slf4j.Logging
import com.redis.serialization.Parse.Implicits.parseInt

import java.io._

import com.amazonaws.services.simpledb.AmazonSimpleDB
import com.amazonaws.services.simpledb.model.{PutAttributesRequest, SelectRequest, ReplaceableAttribute, Item}

import java.text.SimpleDateFormat
import java.sql.Timestamp

import RedisFactory._

import org.joda.time._


object FixBrandFeed extends SquerylHelper with Instrumented with RedisHelper with Logging {

  //Staging Config
  //val redisServer = "54.242.224.74"
  //Production Config
  val redisServer = "redis3.pongrdev.com"
  Settings.redisConfig = Some(RedisConfig(redisServer, 6379, 1))

  val filterDateTime = "2013-04-26 04:50:00"
  //val filterDateTime = "2013-06-07 00:00:00"
  val since = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(filterDateTime).getTime)  

  val clients = new RedisClientPool(redisServer, 6379, database = 1) //production
  val r = {
    clients.withClient { c => c }
  }

  def main(args: Array[String]) {

    try {

      initSqueryl

      inTransaction {

        import com.redis.serialization.Parse.Implicits.parseInt

        val photoIds: Seq[(String, Int)] = from(Schemas.photos)(p => where(p.createdAt.getOrElse(null) >= since) select(p.uuid)).toList.map { uuid =>
          val ids = getPhotoId(uuid).map(id => uuid -> id)
          if(ids.isEmpty) println("Photo Not Found in Redis : " + uuid)
          ids
        } flatten

        val photos = photoIds.map { ids =>
          val (uuid, photoId) = ids
          import com.redis.serialization.Parse.Implicits.parseByteArray
          val photo: Option[PhotoDb] = r.hget[Array[Byte]](photosTable(photoId), photoId).flatMap{p => p}
          if(photo.isEmpty) {
            println("Found a photo ID but not in photos table : %s = %s " format(uuid, photoId))
          }
          photo
        } flatten

        val userIds = photos.map(_.user).distinct
        for(userId <- userIds) {
          val userPhotoIds: List[Long] = r.zrange[Int](userFeedTable(userId), 0, -1).getOrElse(Nil).map(_.toLong)
          for(photoId <- userPhotoIds) {
            if(!r.hexists(photosTable(photoId), photoId)) {
              println("Found a photo in the user feed that doesn't exist in the photos table : user=%s, photo=" format (userId, photoId))
              r.zrem(userFeedTable(userId), photoId)
            }
          }          
        }

        val brandIds = photos.map(_.brands.headOption).flatten.distinct
        for(brandId <- brandIds) {
          val brandPhotoIds: List[Long] = r.zrange[Int](brandFeedTable(brandId), 0, -1).getOrElse(Nil).map(_.toLong)
          for(photoId <- brandPhotoIds) {
            if(!r.hexists(photosTable(photoId), photoId)) {
              println("Found a photo in the brand feed that doesn't exist in the photos table : brand=%s, photo=" format (brandId, photoId))
              r.zrem(brandFeedTable(brandId), photoId)
            }
          }
        }        

        //Get all brand photos from it's feed
        /*val brandPhotoIds: List[Long] = r.zrange[Int](brandFeedTable(brandId), 0, -1).getOrElse(Nil).map(_.toLong)
        println("Feed length : " + brandPhotoIds.size)
        for(photoId <- brandPhotoIds) {
          if(!r.hexists(photosTable(photoId), photoId)) {
            println("Found a photo in the feed that doesn't exist in the photos table : " + photoId)
            r.zrem(brandFeedTable(brandId), photoId)
          }
        }*/

        info("Done...")
    
      }
    } catch {
      case e: Exception =>
        println(e.getMessage)
    }

    System.exit(0)

  }


}