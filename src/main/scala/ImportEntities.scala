package com.pongr

import com.pongr._
import com.redis._

import com.yammer.metrics.scala.Instrumented
import serialization._

import com.pongr.view.model.{PhotoDb => PhotoTable, CommentDb => SqlCommentDb, _}
import com.pongr.view.model.CommentType._
import com.pongr.view.photo._
import com.pongr.view.redis._
import com.pongr.view.common._
import com.pongr.oauth2._
import com.pongr.simpledb._
import java.util.Date
import java.util.UUID._

import org.squeryl._
import org.squeryl.PrimitiveTypeMode._

import com.amazonaws.services.simpledb.AmazonSimpleDBClient
import com.amazonaws.auth.BasicAWSCredentials

import grizzled.slf4j.Logging
import com.redis.serialization.Parse.Implicits.parseInt

import java.io._

import com.amazonaws.services.simpledb.AmazonSimpleDB
import com.amazonaws.services.simpledb.model.{PutAttributesRequest, SelectRequest, ReplaceableAttribute, Item}

import java.text.SimpleDateFormat
import java.sql.Timestamp

import RedisFactory._

object ImportEntities extends SquerylHelper with SimpleDBQuery with Instrumented with RedisHelper with Logging {

  //Staging Config
  /*val redisServer = "redis1.pongrdev.com"
  val brandThemeEnv = "Staging"*/
  //Production Config
  val redisServer = "redis3.pongrdev.com"
  //val redisServer = "184.72.196.39"
  Settings.redisConfig = Some(RedisConfig(redisServer, 6379, 1))
  val brandThemeEnv = "Production"

  //val filterDateTime = "2013-04-26 04:50:00"
  //val filterDateTime = "2013-05-29 00:00:00"
  val filterDateTime = "2013-06-07 00:00:00"
  val since = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(filterDateTime).getTime)

  val clientQuery = new InMemoryClientRepository
  val communityQuery = new InMemoryCommunityRepository

  //override lazy val clients = new RedisClientPool("redis1.pongrdev.com", 6379, database = 1) //staging
  val clients = new RedisClientPool(redisServer, 6379, database = 1) //production
  val r = {
    clients.withClient { c => c }
  }

  //access token query
  def awsCreds = new BasicAWSCredentials("0NC0HFSGC8TJKEREQT82", "0vjx/o5hJM3jNRnX4MsxyVchoSOO4rh1cgs9Fwyl")
  def amazonSimpleDBClient = new AmazonSimpleDBClient(awsCreds)

  def createPhotoId(uuid: String): Int = {
    client { r =>
      val newId = r.incr(lastPhotoIdTable).map(_.toInt) getOrElse {
        println("Couldn't generate Int in Redis for photo = %s" format uuid)
        0
      }
      r.hset(photoIdsTable, uuid, newId)
      newId
    }
  }

  def main(args: Array[String]) {

    try {

      initSqueryl

      inTransaction {

        val divider = 100000
        val userscount = from(Schemas.users)((u) => compute(count))

        println("Users Found: " + userscount.toLong)
        var userIds: Map[Long, String] = Map.empty

        for(i <- 0 to (userscount/divider).toInt) {
          val dbusers = {
            from(Schemas.users)((u) => select(u))
            .page(i*divider, divider)
            .map{ u => {
              User(
                u.id,
                u.uuid,
                ImageUrl(u.avatarUrls),
                u.userName.getOrElse(""),
                u.firstName.getOrElse(""),
                u.lastName.getOrElse(""),
                u.twitterUserId.getOrElse(0l),
                u.facebookUserId.getOrElse("")
              )
            }}
          }

          userIds = userIds ++ Map(dbusers.map{u => (u.id, u.uuid)}.toSeq: _*)

          //add users into user hash in Redis
          if(dbusers.size > 0)
            r.hmset(usersTable, dbusers.map{u => (u.uuid, write(u))})  
          info("imported users... " + i)

        }


        //Import Brands
        import com.pongr.simpledb.Implicits._
        implicit def itemToBrandTheme(item: Item): (String, String) = ((item("brandId") getOrElse "") -> (item("mainColor") getOrElse "#C60021"))
        val brandColors: Map[String, String] = selectMany(amazonSimpleDBClient, true)(itemToBrandTheme)("select brandId, mainColor from brandThemes where env='%s'" format (brandThemeEnv)).toMap
        val dbbrands: List[Brand] = {
        	Schemas.brands.map{ b =>
        		Brand(
        			b.id,
        			b.uuid,
        			ImageUrl(b.avatarUrls),
        			b.username,
        			b.name,
        			brandColors.getOrElse(b.uuid, "#C60021")
        		)
        	}.toList
        }
        r.hmset(brandsTable, dbbrands.map{b => (b.uuid, write(b))})
        info("imported brands.")



        //Read User Followers
        val dbuserfollowers = {
        	join(Schemas.userFollows, Schemas.users, Schemas.users)((uf, u1, u2) =>
            where(uf.createdAt >= since)
            select(u1.uuid, u2.uuid)
            on(uf.likedUser === u1.id, uf.likedByUser === u2.id)
          ).toList
        }



        //Write User Followers&Followings
        var countentity = 0
        for((userId, values) <- dbuserfollowers.groupBy(_._1).toList) {
          if(countentity % 10000 == 0) info("importing user follower : " + countentity)
          values.map(_._2).toList match {
            case head::tail => r.sadd(userFollowingsTable(userId), head, tail: _*)
            case _ =>
          }
          countentity += 1
        }
        info("imported user followers.")



        //Write User Brands
        countentity = 0
        for((userId, values) <- dbuserfollowers.groupBy(_._2).toList) {
          if(countentity % 10000 == 0) info("importing user following : " + countentity)
          values.map(_._1).toList match {
            case head::tail => r.sadd(userFollowersTable(userId), head, tail: _*)
            case _ =>
          }
          countentity += 1     
        }        
        info("imported user followings.")



        //Read User Blocks
        val dbuserblocks = {
          join(Schemas.userBlocks, Schemas.users, Schemas.users)((ub, u1, u2) =>
            where(ub.createdAt >= since)
            select(u1.uuid, u2.uuid)
            on(ub.blockedUser === u1.id, ub.blockedByUser === u2.id)
          ).toList
        }



        //Write User Blocks
        for((userId1, userId2) <- dbuserblocks) {
          r.sadd(userBlocksTable(userId1), userId2)
        }
        info("imported user blocks...")



        //Read Brand Lovers
        val dbbrandlovers = {
          join(Schemas.brandLoves, Schemas.brands, Schemas.users)((bl, b, u) =>
            where(bl.createdAt >= since)
            select(b.uuid, u.uuid)
            on(bl.brand === b.id, bl.user === u.id)
          ).toList
        }



        //Write Brand Lovers
        countentity = 0
        for((brandId, values) <- dbbrandlovers.groupBy(_._1).toList) {
          if(countentity % 10000 == 0) info("importing brand lover : " + countentity)

          values.map(_._2).toList match {
            case head::tail => r.sadd(brandLoversTable(brandId), head, tail: _*)
            case _ =>
          }
          countentity += 1
        }
        info("imported brand lovers.")



        //Write User Brands
        countentity = 0
        for((userId, values) <- dbbrandlovers.groupBy(_._2).toList) {
          if(countentity % 10000 == 0) info("importing user brand : " + countentity)
          values.map(_._1).toList match {
            case head::tail => r.sadd(userBrandsTable(userId), head, tail: _*)
            case _ =>
          }
          countentity += 1
        }
        info("imported user brands.")



        //user block????
        val dbcommentsByPhoto: Map[String, Seq[CommentDb]] = {
          join(Schemas.comments, Schemas.users, Schemas.photos)((c, u, p) =>
            where(c.typeofComment === CommentType.Photo and c.createdAt >= since)          
            select(c, u.uuid, p.uuid)
            on(c.user === u.id, c.commentable === p.id)
          ).toSeq.map{ v =>
            val (c, userId, photoId) = v
            photoId -> CommentDb(c.id, randomUUID.toString, c.text, userId, c.createdAt)        
          }.groupBy(_._1) mapValues (_ map (_._2))
        }



        //val dbcomments: Seq[CommentDb] = dbcommentsByPhoto.values.reduceLeft{_++_}
        for(list <- dbcommentsByPhoto.values.grouped(1000).toList) {
          val dbcomments: Seq[CommentDb] = list.reduceLeft{_++_}
          if(dbcomments.nonEmpty)
            r.hmset(commentsTable, dbcomments.map{c => (c.uuid, write(c))})
        }
        info("imported comments.")



        val lovers: Map[String, Seq[String]] = {
          join(Schemas.photoLikes, Schemas.users, Schemas.photos)((pl, u, p) => 
            select(p.uuid, u.uuid)
            on(pl.user === u.id, pl.photo === p.id)
          ).toSeq.groupBy(_._1) mapValues (_ map (_._2))
        }



        val facebookSharers: Map[String, Seq[String]] = {
          join(Schemas.photoShares, Schemas.users, Schemas.photos)((ps, u, p) =>
            where(ps.site === "Facebook")          
            select(p.uuid, u.uuid)
            on(ps.user === u.id, ps.photo === p.id)
          ).toSeq.groupBy(_._1) mapValues (_ map (_._2))
        }



        val twitterSharers: Map[String, Seq[String]] = {
          join(Schemas.photoShares, Schemas.users, Schemas.photos)((ps, u, p) =>
            where(ps.site === "Twitter")          
            select(p.uuid, u.uuid)
            on(ps.user === u.id, ps.photo === p.id)
          ).toSeq.groupBy(_._1) mapValues (_ map (_._2))
        }

      /*val photoIdsToFix = Seq(
        "0f7b2628-6fa9-4367-94f4-e203c5dcb5c9",
        "d02d809f-5af4-496e-9f87-d324f390b67d",
        "16d3b992-f72a-4d93-aadf-68067a740b84",
        "1aa56d9c-cc4d-49c1-83fa-23bfa384cb91",
        "72513adf-346f-4bef-ad47-d467e0172123",
        "767c40d0-bb61-4929-abe4-e240f627d73f",
        "b14c5b54-cb88-4515-add5-60e67ab7967b",
        "2fce22d3-c160-42cc-b741-8b9c30bee474",
        "33114867-6edf-4678-9c39-7c9540139e85",
        "f2e4d987-92b3-4f94-b51c-da98a63ca863",
        "b4b98220-f794-4cf7-a836-b80fe463d66b",
        "507de2c5-0a52-4bc5-b2f3-30940d0591fc",
        "37461b4d-a1df-4a2a-b32f-7c94027932eb",
        "b2ef619f-d91d-450c-a96c-e9577f434e57",
        "c96f2393-b6f6-4131-b4d9-d5020d30beb7"
      )*/

        val spottedphotos = {from(Schemas.brandTags)(bt => select(bt.photoId))}.toList

        val photoscount = from(Schemas.photos)(p => where(p.createdAt.getOrElse(null) >= since) compute(count))
        val photodivider = 100000

        println("Photos Found: " + photoscount.toLong)

        for(ii <- 0 to (photoscount/photodivider).toInt) {
          val ii = 0
          info("Photo Block - " + ii)
          info("Retrieving photos from MySQL...")

          def photosFromDb: List[PhotoTable] = {
            from(Schemas.photos)((p) =>
              where(p.createdAt.getOrElse(null) >= since)
              select(p)
              orderBy(p.createdAt desc)
            ).page(ii*photodivider, photodivider)
          }.toList

          val dbphotos: List[PhotoDb] = photosFromDb.map{ p =>

            val source = {
              (spottedphotos.contains(p.uuid), p.tweetId.filter{_.nonEmpty}, p.email, p.brandEmail.filter{_.nonEmpty}, p.clientId.filter{_.nonEmpty}) match {
                case (_, Some(_), _, _, _) => Some("Twitter")
                case (true, _, _, _, _) => Some("Facebook")
                case (false, _, Some(email), Some(brandEmail), _) => Some(brandEmail)
                case (false, _, None, Some(brandEmail), _) => Some("Web")
                case (false, _, None, None, Some(clientId)) => clientQuery.getById(clientId).map{_.name}
                case _ => None //we can't determine the source of the photo. Something is WRONG!!!
              }
            }
            //println("User : " + userIds.getOrElse(p.user, ""))
            PhotoDb(
              p.id,
              p.uuid,
              ImageUrl(p.photoUrls),
              p.url.getOrElse(""),
              p.description,
              userIds.getOrElse(p.user, ""),
              Seq(dbbrands.filter{_.id == p.brand}.headOption.map{_.uuid}).flatten,
              dbcommentsByPhoto.getOrElse(p.uuid, Nil).map{_.uuid}.toList, //comments
              lovers.getOrElse(p.uuid, Nil).toList, //photo lovers
              p.tweetId,
              p.location.getOrElse(""),
              source,
              p.clientId,
              p.createdAt.getOrElse(new Date()),
              facebookSharers.getOrElse(p.uuid, Nil).toList,
              twitterSharers.getOrElse(p.uuid, Nil).toList,
              _matchedImage = None
            )
          }
          info("Created Photo models...")

          //val photoIds: Map[String, String] = r.hgetall[String, String](photoIdsTable).get
          //println("IDS: " + photoIds)
          //val photoIds = dbphotos.map{_.uuid}.zipWithIndex.toMap.map{p => (p._1, p._2 + photodivider*ii)}
          //client.hmset(photoIdsTable, photoIds)
          import com.redis.serialization.Parse.Implicits.parseByteArray
          val photoIds: Map[String, Int] = dbphotos.map(p => {
           
            val photoId = getOrCreatePhotoId(p.uuid)
            val photoFromFeed: Option[PhotoDb] = r.hget[Array[Byte]](photosTable(photoId), photoId).flatMap{p => p}
            photoFromFeed match {
              case Some(photo) =>
                //check if photos don't match. If they don't match, fix it
                if(photo.uuid != p.uuid) {
                  //Remove this photo from the user's feed&home feed & brands feed
                  r.zrem(userFeedTable(p.user), photoId)
                  r.zrem(homeFeedTable(p.user), photoId)
                  for(brandId <- p.brands.headOption)
                    r.zrem(brandFeedTable(brandId), photoId)
                  r.hdel(photoIdsTable, p.uuid)
                  println("Fixed photo : " + p.uuid)
                  p.uuid -> createPhotoId(p.uuid) //Fix if this photo has been duplicated...
                } else {
                  p.uuid -> photoId
                }              
              case _ =>
                p.uuid -> photoId
            }

          }).toMap

          for((shardId, ps) <- dbphotos.map{p => ((photoIds(p.uuid))-> write(p))}.groupBy(_._1/1000)) {
            r.hmset("photos:" + shardId, ps)
          }
          /*for(dbphoto <- dbphotos) {
            val photoIdAsInt = getOrCreatePhotoId(dbphoto.uuid)
            if(photoIdAsInt > 1000) { 
              r.hdel(photosTable(photoIdAsInt.toLong), photoIdAsInt)
              r.hset(photosTable(photoIdAsInt.toLong), photoIdAsInt, write(dbphoto))
            }
          }*/

          info("Completed photo block == " + ii)
        }
        
        info("Done...")
    
    }
    } catch {
      case e: Exception =>
        println(e.getMessage)
    }

    System.exit(0)

  }




}