package com.pongr

import com.pongr._
import com.redis._

import com.yammer.metrics.scala.Instrumented
import serialization._

import com.pongr.view.model.{PhotoDb => PhotoTable, CommentDb => SqlCommentDb, _}
import com.pongr.view.model.CommentType._
import com.pongr.view.photo._
import com.pongr.view.redis._
import com.pongr.view.common._
import com.pongr.oauth2._
import com.pongr.simpledb._
import java.util.Date
import java.util.UUID._

import org.squeryl._
import org.squeryl.PrimitiveTypeMode._

import com.amazonaws.services.simpledb.AmazonSimpleDBClient
import com.amazonaws.auth.BasicAWSCredentials

import grizzled.slf4j.Logging
import com.redis.serialization.Parse.Implicits.parseInt

import java.io._

import com.amazonaws.services.simpledb.AmazonSimpleDB
import com.amazonaws.services.simpledb.model.{PutAttributesRequest, SelectRequest, ReplaceableAttribute, Item}

import java.text.SimpleDateFormat
import java.sql.Timestamp

import RedisFactory._

import org.joda.time._


object FixMergedBrands extends SquerylHelper with SimpleDBQuery with Instrumented with RedisHelper with Logging {

  //Staging Config
  /*val redisServer = "redis1.pongrdev.com"
  "*/
  //Production Config
  val redisServer = "redis3.pongrdev.com"

  val clientQuery = new InMemoryClientRepository
  val communityQuery = new InMemoryCommunityRepository

  val filterDateTime = "2013-04-26 04:50:00"
  val since = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(filterDateTime).getTime)

  val clients = new RedisClientPool(redisServer, 6379, database = 1) //production
  val r = {
    clients.withClient { c => c }
  }


  val excludedBrands: Seq[String] = r.smembers[String](excludedBrandsTable).map{_.toSeq.flatten}.getOrElse{Nil} 
  val problemUsers: Seq[String] = r.smembers[String](problemUsersTable).map{_.toSeq.flatten}.getOrElse{Nil}

  //access token query
  def awsCreds = new BasicAWSCredentials("0NC0HFSGC8TJKEREQT82", "0vjx/o5hJM3jNRnX4MsxyVchoSOO4rh1cgs9Fwyl")
  def amazonSimpleDBClient = new AmazonSimpleDBClient(awsCreds)

  def main(args: Array[String]) {

    try {

      val photoIdsToFix = Seq(
        "0f7b2628-6fa9-4367-94f4-e203c5dcb5c9",
        "d02d809f-5af4-496e-9f87-d324f390b67d",
        "16d3b992-f72a-4d93-aadf-68067a740b84",
        "1aa56d9c-cc4d-49c1-83fa-23bfa384cb91",
        "72513adf-346f-4bef-ad47-d467e0172123",
        "767c40d0-bb61-4929-abe4-e240f627d73f",
        "b14c5b54-cb88-4515-add5-60e67ab7967b",
        "2fce22d3-c160-42cc-b741-8b9c30bee474",
        "33114867-6edf-4678-9c39-7c9540139e85",
        "f2e4d987-92b3-4f94-b51c-da98a63ca863",
        "b4b98220-f794-4cf7-a836-b80fe463d66b",
        "507de2c5-0a52-4bc5-b2f3-30940d0591fc",
        "37461b4d-a1df-4a2a-b32f-7c94027932eb",
        "b2ef619f-d91d-450c-a96c-e9577f434e57",
        "c96f2393-b6f6-4131-b4d9-d5020d30beb7"
      )


      initSqueryl

      inTransaction {
      
        //get photos from Redis
        val photoIds: Map[String, String] = r.hgetall[String, String](photoIdsTable).get
        val brandIds: Map[Long, String] = from(Schemas.brands)((b) => select(b.id, b.uuid)).toMap
        val photos = from(Schemas.photos)(p => where(p.createdAt.getOrElse(null) >= since) select((p.uuid, p.brand))).toList.map { p =>
          val (photoId, brandIdAsInt) = p
          brandIds.get(brandIdAsInt).map(brandId => photoId -> brandId)
        }.flatten

        for((photoId, brandId) <- photos) {
          for(photoIdAsInt <- photoIds.get(photoId)) {
            import com.redis.serialization.Parse.Implicits.parseByteArray
            val photoOption: Option[PhotoDb] = r.hget[Array[Byte]](photosTable(photoIdAsInt.toLong), photoIdAsInt).flatMap(p => p)

            for(photo <- photoOption) {
              if(photo.brands.head != brandId) {
                val oldBrandId = photo.brands.head
                //update the photo's brand
                r.hset(photosTable(photoIdAsInt.toLong), photoIdAsInt, write(photo.copy(brands = Seq(brandId))))
                //add the photo into the new brand's feed
                r.zadd(brandFeedTable(brandId), photo.createdAt.getTime, photoIdAsInt)
                //add the photo's user into the brand's lover
                r.sadd(brandLoversTable(brandId), photo.user)
                //update the photo's user's brands
                r.srem(userBrandsTable(photo.user), oldBrandId)
                r.sadd(userBrandsTable(photo.user), brandId)

                //add the photo into the brand's lovers home feeds
                val brandLovers: List[String] = r.smembers[String](brandLoversTable(brandId)).map{_.flatten}.getOrElse(Nil).toList
                for(loverId <- brandLovers) {
                  r.zadd(homeFeedTable(loverId), photo.createdAt.getTime, photoIdAsInt)
                }

                //finally, update the photo's users home feed
                val brandPhotos: Seq[(String, Double)] = r.zrangeWithScore[String](brandFeedTable(brandId), 0, -1) getOrElse Nil
                brandPhotos.take(1000).map(_.swap).toList match {
                  case head::tail => r.zadd(homeFeedTable(photo.user), head._1, head._2, tail: _*)
                  case _ =>
                }

                r.del(brandFeedTable(oldBrandId))
                r.del(brandLoversTable(oldBrandId))

                println("Fixed photo : " + photoId)
              }
            }
          }
        }

        info("Done...")
    
      }
    } catch {
      case e: Exception =>
        println(e.getMessage)
    }

    System.exit(0)

  }


}