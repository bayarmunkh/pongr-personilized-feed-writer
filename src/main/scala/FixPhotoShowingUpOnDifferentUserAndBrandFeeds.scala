package com.pongr

import com.pongr._
import com.redis._

import com.yammer.metrics.scala.Instrumented
import serialization._

import com.pongr.view.model.{PhotoDb => PhotoTable, CommentDb => SqlCommentDb, _}
import com.pongr.view.model.CommentType._
import com.pongr.view.photo._
import com.pongr.view.redis._
import com.pongr.view.common._
import com.pongr.oauth2._
import com.pongr.simpledb._
import java.util.Date
import java.util.UUID._

import org.squeryl._
import org.squeryl.PrimitiveTypeMode._

import com.amazonaws.services.simpledb.AmazonSimpleDBClient
import com.amazonaws.auth.BasicAWSCredentials

import grizzled.slf4j.Logging
import com.redis.serialization.Parse.Implicits.parseInt

import java.io._

import com.amazonaws.services.simpledb.AmazonSimpleDB
import com.amazonaws.services.simpledb.model.{PutAttributesRequest, SelectRequest, ReplaceableAttribute, Item}

import java.text.SimpleDateFormat
import java.sql.Timestamp

import RedisFactory._

import org.joda.time._


object FixPhotoShowingUpOnDifferentUserAndBrandFeeds extends SquerylHelper with SimpleDBQuery with Instrumented with RedisHelper with Logging {

  //Staging Config
  /*val redisServer = "redis1.pongrdev.com"
  "*/
  //Production Config
  val redisServer = "redis3.pongrdev.com"
  Settings.redisConfig = Some(RedisConfig(redisServer, 6379, 1))  

  val clientQuery = new InMemoryClientRepository
  val communityQuery = new InMemoryCommunityRepository

  val filterDateTime = "2013-04-26 04:50:00"
  val since = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(filterDateTime).getTime)

  val clients = new RedisClientPool(redisServer, 6379, database = 1) //production
  val r = {
    clients.withClient { c => c }
  }


  val excludedBrands: Seq[String] = r.smembers[String](excludedBrandsTable).map{_.toSeq.flatten}.getOrElse{Nil} 
  val problemUsers: Seq[String] = r.smembers[String](problemUsersTable).map{_.toSeq.flatten}.getOrElse{Nil}



  //access token query
  def awsCreds = new BasicAWSCredentials("0NC0HFSGC8TJKEREQT82", "0vjx/o5hJM3jNRnX4MsxyVchoSOO4rh1cgs9Fwyl")
  def amazonSimpleDBClient = new AmazonSimpleDBClient(awsCreds)

  def main(args: Array[String]) {

    try {


      initSqueryl

      inTransaction {

        /*val fixPhotoIds = Seq(
          "0fe9f056-5cc5-4d63-a336-cab141933553",
          "69a0127e-dbe6-47f8-9459-d7a112bee347",
          "aecfad49-9d53-4618-ad55-1fcebc3c4884",
          "82ab4421-9b2a-4e8c-9ca3-fbdb4a7dab03",
          "76294f31-00c1-441c-9349-1bf287294938",
          "74e55472-b381-4db6-a2a3-e418ea94dbaa",
          "8a0840c3-4431-4b65-8a8a-1f61ce6ff019",
          "fa10c35d-7e40-4f77-8199-f56880b1da9b",
          "5d546cb8-fd89-4aff-9073-50930ccf8d05",
          "5b8fac8c-43fc-41ee-998c-92e1aaf249f1",
          "13871043-3dd0-43d5-9ead-54837d58edfc",
          "95d01800-4f0a-4550-99da-d01012e41fdb",
          "ebc1cb56-c29c-4237-b55c-f642d85fbbe9",
          "a7d434bf-20da-4f67-beb7-5f415706acc6",
          "4d3a502d-6248-4015-8aa2-4493ad8cbf84",
          "c5708966-8628-4fe9-8665-f03e2170e15b",
          "5d0aec27-aeb5-4bf1-b430-c82fb73836ab",
          "4e07009d-fcf2-43b4-a5f9-37e5ee84f4c5",
          "407e98db-fc5c-4f8f-b7d9-135fd514d2eb",
          "5244d489-dc3c-4039-8b76-d2092dee3fcd",
          "e012c053-64e6-4a83-9a44-45b4da86abd9",
          "98681c87-fc53-494b-afb4-8413deda719d",
          "c99ad850-70eb-4e54-80d2-74b5f5c7c129",
          "854f5cc6-f665-435f-8863-602492f4b9b9",
          "897f44a9-ae1a-42c0-9c69-23ad9ee4fe7e",
          "ff7297f8-18ef-48c2-bf34-a9d36850387b"
        )*/

        /*def createPhotoId(uuid: String): Int = {
          import com.redis.serialization.Parse.Implicits.parseInt

          val lock = new Object
          lock.synchronized {
              def getNextId(id: Int): Int = {
                if(r.hexists(photosTable(id), id)) {
                  getNextId(id + 1)
                } else {
                  id
                }
              }

              val newId = getNextId(r.hlen(photoIdsTable).getOrElse(0l).toInt + 1)
              r.hset(photoIdsTable, uuid, newId)
              newId
          }
        }*/
// or p.user in List(947056l)
        import com.redis.serialization.Parse.Implicits.parseInt
        val photoIds: Seq[(String, Int)] = from(Schemas.photos)(p => where(p.createdAt.getOrElse(null) >= since) select(p.uuid)).toList.map { uuid =>
          val ids = getPhotoId(uuid).map(id => uuid -> id)
          if(ids.isEmpty)
            println("Photo Not Found in Redis : " + uuid)
          ids
        } flatten
        //val photoIds: Seq[Long] = r.hmget[String, Int](photoIdsTable, fixPhotoIds: _*).map(_.values.toSeq).getOrElse(Nil).map(_.toLong)
        val photos = photoIds.map { ids =>
          val (uuid, photoId) = ids
          import com.redis.serialization.Parse.Implicits.parseByteArray
          val photo: Option[PhotoDb] = r.hget[Array[Byte]](photosTable(photoId), photoId).flatMap{p => p}
          if(photo.isEmpty) {
            println("Found a photo ID but not in photos table : %s = %s " format(uuid, photoId))
            //r.hset(photoIdsTable, uuid, createPhotoId(uuid))
          }
          photo
        } flatten

        //for(photo <- photos) {
        val photosByUser = photos.groupBy(_.user)
        for(userId <- photosByUser.keys) {
          val userPhotoIds: List[Long] = r.zrange[Int](userFeedTable(userId), 0, -1).getOrElse(Nil).map(_.toLong)
          for(photoId <- userPhotoIds) {
            import com.redis.serialization.Parse.Implicits.parseByteArray
            val photoFromFeedOption: Option[PhotoDb] = r.hget[Array[Byte]](photosTable(photoId), photoId).flatMap{p => p}
            for(photoFromFeed <- photoFromFeedOption) {
              if(photoFromFeed.user != userId) {
                println("Found Photo to fix : originaluser=%s user=%s photo=%s".format(userId, photoFromFeed.user, photoFromFeed.uuid))
                r.zrem(userFeedTable(userId), photoId)
              }              
            }
          }          
        }

        val photosByBrand = photos.groupBy(_.brands.head)
        for(brandId <- photosByBrand.keys) {
          val brandPhotoIds: List[Long] = r.zrange[Int](brandFeedTable(brandId), 0, -1).getOrElse(Nil).map(_.toLong)
          for(photoId <- brandPhotoIds) {
            import com.redis.serialization.Parse.Implicits.parseByteArray
            val photoFromFeedOption: Option[PhotoDb] = r.hget[Array[Byte]](photosTable(photoId), photoId).flatMap{p => p}
            for(photoFromFeed <- photoFromFeedOption) {
              if(photoFromFeed.brands.head != brandId) {
                println("Found Photo to fix : originalbrand=%s brand=%s photo=%s".format(brandId, photoFromFeed.brands.head, photoFromFeed.uuid))
                r.zrem(brandFeedTable(brandId), photoId)
              }              
            }
          }
        }

        /*val photoIds: Map[String, String] = r.hgetall[String, String](photoIdsTable).get
        val photos = from(Schemas.photos)(p => where(p.user === 9) select((p.uuid, p.brand, p.user, p.createdAt.map(_.getTime.toDouble).getOrElse(0d))) orderBy(p.createdAt desc)).toList
        //where(u.id in List(1, 7, 11, 799, 849))
        val userIds: Map[Long, String] = from(Schemas.users)((u) => where(u.id in List(9)) select(u.id, u.uuid) orderBy(u.createdAt asc)).toMap//.toSeq.sortWith((a,b) => photoUserIds.indexOf(a._1) < photoUserIds.indexOf(b._1)).toMap
        //println("Make sure users are ordered correctly : " + userIds.take(10))
        val userIntIds: Map[String, Long] = from(Schemas.users)((u) => select(u.uuid, u.id)).toMap

        //Importing User Feeds
        var countentity = 0
        val userPhotos: Map[Long, Seq[(String, Long, Long, Double)]] = photos.groupBy(_._3)
        info("User feeds count : " + userPhotos.size)
        for((userId, userPhotos) <- userPhotos.toList) {
          if(countentity % 10000 == 0) info("importing user feed : " + countentity)
          userPhotos.map{p => (p._4, p._1)}.toList match {
            case ps if(ps.size > 0) =>
              val head::tail = ps
              for((score, photoId) <- ps) {
                val photoIdAsInt = getOrCreatePhotoId(photoId)
                //r.hset(photosTable(photoIdAsInt), )
                r.zadd(userFeedTable(userIds.getOrElse(userId, "null")), score, photoIdAsInt)
              }
              //r.zadd(userFeedTable(userIds.getOrElse(userId, "null")), head._1, photoIds.getOrElse(head._2, 0), tail.map{p => (p._1, photoIds.getOrElse(p._2, 0))}: _*)
            case _ =>
          }
          countentity += 1
        }     
        info("imported user feeds.")        */

        info("Done...")
    
    }
    } catch {
      case e: Exception =>
        e.printStackTrace
    }

    System.exit(0)

  }


}