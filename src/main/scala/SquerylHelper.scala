package com.pongr

import org.squeryl._
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.annotations._
import org.squeryl.dsl._
import org.squeryl.{ SessionFactory, Session }
import org.squeryl.adapters.MySQLAdapter

import java.sql.DriverManager

trait SquerylHelper {


	def initSqueryl() {
        val url =  "jdbc:mysql://production55.c0ykv6bb9z8i.us-east-1.rds.amazonaws.com:3306/game"
        //val url =  "jdbc:mysql://staging55.c0ykv6bb9z8i.us-east-1.rds.amazonaws.com:3306/game"
        val user = "game"
        val password = "game"
        val adapter = new MySQLAdapter

        Class.forName("com.mysql.jdbc.Driver").newInstance()
        SessionFactory.concreteFactory = Some(() => {
          val session = Session.create(DriverManager.getConnection(url, user, password), adapter)
          //session.setLogger(msg => println(msg))
          session
        })
        println("Squeryl DB setup complete")
  }


}